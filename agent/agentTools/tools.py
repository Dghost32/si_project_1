import numpy as np
import collections
import copy
from collections import deque

symbol = {"wall": 0, "floor": 1, "diamond": 2,
          "spike": 3, "key": 4, "door": 5, "exit": 9}
number = {0: "wall", 1: "floor", 2: "diamond",
          3: "spike", 4: "key", 5: "door", 9: "exit"}

walkMap = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]


def mapFirstLevel():
  return [['W', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'W'],
          ['W', 'W', 'F', 'K', 'K', 'K', 'K', 'F', 'D', 'W'],
          ['W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W'],
          ['W', 'F', 'F', 'W', 'W', 'W', 'W', 'W', 'W', 'W'],
          ['W', 'F', 'F', 'W', 'W', 'W', 'W', 'W', 'W', 'W'],
          ['W', 'F', 'P', 'D', 'D', 'D', 'F', 'F', 'F', 'W'],
          ['W', 'W', 'W', 'W', 'W', 'W', 'W', 'D', 'F', 'W'],
          ['W', 'W', 'W', 'W', 'W', 'W', 'W', 'D', 'F', 'W'],
          ['W', 'F', 'D', 'F', 'D', 'D', 'F', 'F', 'F', 'W'],
          ['W', 'F', 'D', 'F', 'W', 'W', 'W', 'W', 'W', 'W'],
          ['W', 'F', 'D', 'F', 'W', 'W', 'W', 'W', 'W', 'W'],
          ['W', 'W', 'W', 'F', 'D', 'D', 'F', 'F', 'F', 'W'],
          ['W', 'W', 'W', 'F', 'F', 'W', 'F', 'E', 'F', 'W'],
          ['W', 'F', 'F', 'F', 'F', 'W', 'F', 'F', 'F', 'W'],
          ['W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W']]


def mapSecondLevelA():
  return [['W', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'W'],
          ['W', 'W', 'F', 'K', 'K', 'K', 'K', 'F', 'D', 'W'],
          ['W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W'],
          ['W', 'F', 'F', 'D', 'F', 'F', 'W', 'L', 'L', 'W'],
          ['W', 'F', 'D', 'F', 'D', 'F', 'W', 'L', 'L', 'W'],
          ['W', 'S', 'W', 'W', 'W', 'F', 'W', 'W', 'F', 'W'],
          ['W', 'D', 'L', 'L', 'W', 'D', 'F', 'D', 'F', 'W'],
          ['W', 'D', 'L', 'L', 'W', 'D', 'F', 'D', 'F', 'W'],
          ['W', 'F', 'W', 'W', 'W', 'S', 'W', 'L', 'L', 'W'],
          ['W', 'F', 'W', 'F', 'D', 'S', 'W', 'L', 'L', 'W'],
          ['W', 'F', 'W', 'E', 'W', 'S', 'W', 'W', 'W', 'W'],
          ['W', 'F', 'W', 'W', 'W', 'F', 'F', 'F', 'F', 'W'],
          ['W', 'D', 'D', 'S', 'S', 'F', 'F', 'P', 'F', 'W'],
          ['W', 'D', 'D', 'W', 'W', 'F', 'F', 'F', 'F', 'W'],
          ['W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W']]


def mapSecondLevelB():
  return [['W', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'W'],
          ['W', 'W', 'F', 'K', 'K', 'K', 'K', 'F', 'D', 'W'],
          ['W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W'],
          ['W', 'F', 'F', 'D', 'F', 'F', 'W', 'L', 'L', 'W'],
          ['W', 'F', 'D', 'F', 'D', 'F', 'W', 'L', 'L', 'W'],
          ['W', 'S', 'W', 'W', 'W', 'F', 'W', 'W', 'F', 'W'],
          ['W', 'D', 'L', 'L', 'W', 'D', 'F', 'D', 'F', 'W'],
          ['W', 'D', 'L', 'L', 'W', 'D', 'F', 'D', 'F', 'W'],
          ['W', 'F', 'W', 'W', 'W', 'S', 'W', 'L', 'L', 'W'],
          ['W', 'F', 'W', 'F', 'D', 'S', 'W', 'L', 'L', 'W'],
          ['W', 'F', 'W', 'E', 'W', 'S', 'W', 'W', 'W', 'W'],
          ['W', 'F', 'W', 'W', 'W', 'P', 'F', 'F', 'F', 'W'],
          ['W', 'D', 'D', 'S', 'S', 'F', 'F', 'F', 'F', 'W'],
          ['W', 'D', 'D', 'W', 'W', 'F', 'F', 'F', 'F', 'W'],
          ['W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W']]


def mapThidrLevel():
  return [['W', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'W'],
          ['W', 'W', 'F', 'K', 'K', 'K', 'K', 'F', 'D', 'W'],
          ['W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W'],
          ['W', 'F', 'F', 'F', 'W', 'F', 'W', 'F', 'D', 'W'],
          ['W', 'P', 'F', 'W', 'W', 'F', 'KD', 'F', 'E', 'W'],
          ['W', 'F', 'F', 'S', 'F', 'F', 'W', 'F', 'D', 'W'],
          ['W', 'W', 'S', 'W', 'KD', 'W', 'W', 'W', 'W', 'W'],
          ['W', 'F', 'F', 'W', 'F', 'F', 'W', 'D', 'D', 'W'],
          ['W', 'F', 'D', 'W', 'F', 'F', 'KD', 'K', 'D', 'W'],
          ['W', 'W', 'F', 'W', 'F', 'F', 'W', 'D', 'D', 'W'],
          ['W', 'K', 'F', 'W', 'D', 'F', 'W', 'W', 'W', 'W'],
          ['W', 'F', 'W', 'W', 'F', 'F', 'W', 'F', 'F', 'W'],
          ['W', 'D', 'S', 'S', 'F', 'F', 'KD', 'K', 'D', 'W'],
          ['W', 'F', 'W', 'W', 'W', 'K', 'W', 'F', 'F', 'W'],
          ['W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W', 'W']]


def pathToTravel(matrix, path):
  m = copy.deepcopy(matrix)
  for x in path:
    m[x[0]][x[1]] = 1
  return m


def printMatrix(matrix):
  print(np.matrix(matrix))


def joinPaths(path1, path2):
  for x in path2:
    if x != path2[0]:
      path1.append(x)

  return path1

# def pathToExit(self, matrix, playerPos, exitSymbol):
#   path = use.bfsPath(matrix, playerPos, exitSymbol, use.symbol["wall"])
#   path_list = []
#   if path != -1:
#     for x in path:
#       if x not in path_list:
#         path_list.append(x)

#   if 0 < len(path_list):
#     return path_list
#   else:
#     return []


def markMultiple(matrix, positionsToMark, mark):
  if len(positionsToMark) > 0:
    changed_matrix = matrix
    for x in positionsToMark:
      changed_matrix[x[0]][x[1]] = mark

    return changed_matrix

  return matrix


def markSingle(matrix, positionToMark, mark):
  new_matrix = matrix
  new_matrix[positionToMark[0]][positionToMark[1]] = mark
  return new_matrix


def getSymbol(matrix, symbolPos):
  return matrix[symbolPos[0]][symbolPos[1]]


def createBinaryMatrix(matrix, filter):
  m = copy.deepcopy(matrix)
  for i, matrix_i in enumerate(matrix):
    for j, value in enumerate(matrix_i):
      if i < 2:
        m[i][j] = 0
      else:
        if value in filter:
          m[i][j] = 1
        else:
          m[i][j] = 0

  return m


# search in the matrix


def findInMatrix(matrix, element):
  for i, matrix_i in enumerate(matrix):
    for j, value in enumerate(matrix_i):
      if value == element:
        return (i, j)
  return ()


def existClearPath(grid, startPosition, goalPosition):
  width = len(grid[0])
  height = len(grid)
  queue = collections.deque([[startPosition]])
  seen = set([startPosition])

  y, x = startPosition
  path = []
  while len(queue) > 0:
    path = queue.popleft()
    x, y = path[-1]

    if x == goalPosition[0] and y == goalPosition[1]:
      return True

    for x2, y2 in [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]:
      if 0 <= x2 < height and 0 <= y2 < width:
        if (x2, y2) not in seen:
          if grid[x2][y2] != 0:
            queue.append(path + [(x2, y2)])
            seen.add((x2, y2))

  return False


def getDirections(path):
  start = path[0]
  #reference_moves = ["L", "R", "U", "D"]
  directions = []
  for x in path:
    if start[0] == x[0] and start[1] < x[1]:
      directions.append("R")

    if start[0] == x[0] and start[1] > x[1]:
      directions.append("L")

    if start[0] < x[0] and start[1] == x[1]:
      directions.append("D")

    if start[0] > x[0] and start[1] == x[1]:
      directions.append("U")

    start = x

  frecuences = []

  count = 0
  current_letter = directions[0]
  next_letter = ""
  for i in range(len(directions)):
    next_letter = directions[i]
    if current_letter != next_letter:
      frecuences.append([count, current_letter])
      current_letter = next_letter
      count = 0

    count = count + 1

    if i == len(directions) - 1:
      frecuences.append([count, current_letter])

  return frecuences


def countObject(matrix, element):
  count = 0
  no_count = 0
  positions = []
  for i, matrix_i in enumerate(matrix):
    no_count = no_count + 1
    for j, value in enumerate(matrix_i):
      if value == element and no_count > 2:
        count = count + 1
        positions.append((i, j))

  if len(positions) > 0:
    return positions
  else:
    return []


def bfsPath(grid, startPosition, goalSymbols, invalidPositions, wallSymbols):
  width = len(grid[0])
  height = len(grid)
  queue = collections.deque([[startPosition]])
  seen = set([startPosition])

  y, x = startPosition
  # print("startPosition", startPosition)
  path = []
  while len(queue) > 0:
    path = queue.popleft()
    x, y = path[-1]

    if grid[x][y] in goalSymbols:
      return path

    for x2, y2 in [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]:
      if 0 <= x2 < height and 0 <= y2 < width:
        if len(invalidPositions) > 0:
          if (x2, y2) not in seen and (x2, y2) not in invalidPositions:
            if grid[x2][y2] not in wallSymbols:
              queue.append(path + [(x2, y2)])
              seen.add((x2, y2))
        else:
          if (x2, y2) not in seen:
            if grid[x2][y2] not in wallSymbols:
              queue.append(path + [(x2, y2)])
              seen.add((x2, y2))

  return []
