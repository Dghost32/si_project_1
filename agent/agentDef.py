# import agentTools.tools as use
import agent.agentTools.tools as use


##########################################
# CLASS SEGMENT
##########################################
class Segment:
  def __init__(self, newData, newPosition):
    if not self.update(newData, newPosition):
      self.head = ()
      self.tail = ()
      self.data = []

  def update(self, newData, newPosition):
    self.position = newPosition
    if len(newData) > 0:
      self.head = newData[0]
      self.tail = newData[-1]
      self.data = newData

      return True

    return False

  def getPos(self):
    return self.position

  def getData(self):
    return self.data

  def getHead(self):
    return self.head

  def getTail(self):
    return self.tail

##########################################
# CLASS SEGPATH
##########################################


class SegPath:
  def __init__(self):
    self.number_of_segments = 0
    self.path = []

  def add(self, newSegment):
    if len(newSegment) > 0:
      self.number_of_segments = self.number_of_segments + 1
      segment_to_add = Segment(newSegment, self.number_of_segments)
      self.path.append(segment_to_add)

      return True

    return False

  def getSegment(self, number):
    if number <= self.number_of_segments:
      for seg in self.path:
        if seg.getPos() == number:
          return seg

    else:
      # segment is empty
      return []

  def getNumSegments(self):
    return self.number_of_segments

  def segTail(self, segment_number):
    if segment_number <= self.number_of_segments:
      return self.path.getTail()

    # not valid segement_number
    return []

  def segHead(self):
    if segment_number <= self.number_of_segments:
      return self.path.getHead()

    # not valid segement_number
    return []

  def fullPath(self):
    if len(self.path) > 0:
      path_to_return = []
      for seg in self.path:
        for element in seg.getData():
          path_to_return.append(element)

      return path_to_return

    return []


##########################################
# CLASS MEMBER
##########################################
class Member:
  def __init__(self):
    self.playerPos = ()
    self.diamondPositions = [()]
    # self.allDiamonds = [()]
    self.doorPositions = [()]
    self.keyPositions = [()]
    self.spikePositions = [()]
    self.holePositions = [()]
    self.keyCount = 0
    self.hasKey = False
    self.objectives = {"keys": 0, "diamonds": 0, "doors": 0, "total": 0}

  def isDiamondPathValid(self, path):
    # print(f"Checking path: \n\n\n{self.diamondPositions}\n\n\n")
    counter = 0
    for diamond in self.diamondPositions:
      if diamond in path:
        counter += 1
    return counter == self.diamondCount

  def searchForObjectives(self, matrix):
    self.objectives["keys"] = len(self.keyPositions)
    self.objectives["diamonds"] = len(self.diamondPositions)
    self.objectives["door"] = len(self.doorPositions)
    self.objectives["total"] = len(
        self.keyPositions) + len(self.diamondPositions) + len(self.doorPositions)

  ########################################################
  # TRAVERSAL MAP
  ########################################################
  def traversalMap(self, matrix):

    dmatrix = use.createBinaryMatrix(matrix, ['P', 'F', 'D', 'S', 'K'])

    use.markMultiple(dmatrix, self.diamondPositions, use.symbol["diamond"])
    use.markMultiple(dmatrix, self.keyPositions, use.symbol["key"])
    use.markMultiple(dmatrix, self.doorPositions, use.symbol["door"])

    level_goals = [
        use.symbol["key"],
        use.symbol["diamond"],
        use.symbol["door"],
    ]

    path_found = SegPath()

    objects_reached = []
    mark_time = 0
    remain_time = 0
    attempt = 0

    while True:
      current_pos = self.playerPos
      keys_remain = self.objectives["keys"]
      diamonds_remain = self.objectives["diamonds"]
      doors_remain = self.objectives["doors"]

      bmatrix = use.copy.deepcopy(dmatrix)

      ################################################

      if attempt > 20:
        print("Not posible path found")
        return []

      attempt = attempt + 1
      attempt_finished = False

      mark_time = len(objects_reached)
      path_found = SegPath()
      path_to_object = []

      #################################################

      doors = use.copy.deepcopy(self.doorPositions)
      keys = use.copy.deepcopy(self.keyPositions)
      activeFilter = doors if not self.hasKey else keys

      print(self.objectives["total"])

      # print(f"Active filter: {activeFilter}")
      for _ in range(self.objectives["total"]):

        if mark_time > 0:
          print(f"objects_reached: {objects_reached}")
          path_to_object = use.bfsPath(
              bmatrix, current_pos, level_goals, objects_reached, [use.symbol["wall"]])
          # mark_time = mark_time - 1
          mark_time = 0
        else:
          path_to_object = use.bfsPath(
              bmatrix, current_pos, level_goals, activeFilter, [use.symbol["wall"]])

        # check for symbol found and decrease the respective objective count
        if len(path_to_object) > 0:

          object_found = path_to_object[-1]

          if not attempt_finished:
            if object_found not in objects_reached:
              attempt_finished = True
              objects_reached.append(object_found)

          #########################
          # DIAMOND PROCESS
          if use.getSymbol(dmatrix, object_found) == use.symbol["diamond"]:

            diamonds_remain = diamonds_remain - 1

            # mark the found item and update the current position
            use.markSingle(bmatrix, object_found, "X")
            current_pos = object_found

            # add the segment found
            path_found.add(path_to_object)

            # mark the spikes found in the segment as wall
            if len(self.spikePositions) > 0:
              for spike in self.spikePositions:
                if spike in path_to_object:
                  use.markSingle(bmatrix, spike, use.symbol["wall"])

          # END OF DIAMOND PROCESS
          #########################

          #########################
          # KEY PROCESS
          if use.getSymbol(dmatrix, object_found) == use.symbol["key"]:
            self.hasKey = True

            keys.remove(object_found)
            activeFilter = keys

            # print("Key found")
            # if not self.hasKey:
            keys_remain = keys_remain - 1

            # mark the found item and update the current position
            use.markSingle(bmatrix, object_found, "X")

            current_pos = object_found

            # add the segment found
            path_found.add(path_to_object)

            # mark the spikes found in the segment as wall
            if len(self.spikePositions) > 0:
              for spike in self.spikePositions:
                if spike in path_to_object:
                  use.markSingle(bmatrix, spike, use.symbol["wall"])

          # END OF KEY PROCESS
          #########################

          #########################
          # DOOR PROCESS

          if use.getSymbol(dmatrix, object_found) == use.symbol["door"] and self.hasKey:
            self.hasKey = False
            doors_remain = doors_remain - 1

            doors.remove(object_found)
            activeFilter = doors

            # mark the found item and update the current position
            use.markSingle(bmatrix, object_found, "X")
            current_pos = object_found

            # add the segment found
            path_found.add(path_to_object)

            # mark the spikes found in the segment as wall
            if len(self.spikePositions) > 0:
              for spike in self.spikePositions:
                if spike in path_to_object:
                  use.markSingle(bmatrix, spike, use.symbol["wall"])

          # END OF DOOR PROCESS
          #########################

          # print(f"keys_remain {keys_remain}")
          use.printMatrix(bmatrix)

        else:
          # broken path
          # mark_time += 1
          break

        # use.printMatrix(bmatrix)
        # print("diamonds reamian: "+str(diamonds_remain))

      # check for complete path + diamonds_remain + doors_remain
      complete_objectives = keys_remain + diamonds_remain + doors_remain
      if complete_objectives <= 0:
        start_position = self.playerPos
        if path_found.getNumSegments():
          start_position = path_found.fullPath()[-1]

        path_to_exit = use.bfsPath(
            matrix, start_position, "E", [], "W")
        use.printMatrix(matrix)
        path_found.add(path_to_exit)
        break

    # print(path_found.fullPath())
    # print("objects: " + str(self.objectives["total"]))
    return path_found.fullPath()

###################################################

  ##########################################
  # SETUP
  ##########################################

  def setupMapInfo(self, matrix):
    self.diamondPositions = use.countObject(matrix, 'D')
    self.doorPositions = use.countObject(matrix, 'KD')
    self.keyPositions = use.countObject(matrix, 'K')
    self.spikePositions = use.countObject(matrix, 'S')
    self.playerPos = use.findInMatrix(matrix, 'P')
    if not self.playerPos:
      self.playerPos = use.findInMatrix(matrix, 'PK')
      print("Player has key")
      print(self.playerPos)
      self.hasKey = True
    # self.spikePositions.append(self.playerPos)
    self.mapExit = use.findInMatrix(matrix, 'E')

    self.searchForObjectives(matrix)

  ##########################################
  # COMPUTE
  ##########################################

  def compute(self, matrix):

    self.setupMapInfo(matrix)
    # full_path = self.diamondPath(matrix)
    full_path = self.traversalMap(matrix)
    if len(full_path) > 0:
      return use.getDirections(full_path)

    return []


if __name__ == "__main__":
  test = Member()
  # m = use.mapFirstLevel()
  # path = test.compute(m)

  m = use.mapSecondLevelB()
  path = test.compute(m)

  # printMatrix(path)
