import agent.agentDef as agent
import envSetup.includeConfig as envS


class Environment:
  def __init__(self):
    self.agent = agent.Member()
    print("Environment init")

  def execute(self):
    while(envS.playing):
      print("-----MENU-----")
      print("1. Capture and process map")
      print("2. Play")
      print("3. Exit")
      option = input("Select an option: ").lower()
      if(option == "1"):
        print("Capturing map...")
        envS.gameCanvas.screenshot("./media/original/map.png")
        img = envS.Image.open("./media/original/map.png")
        # width, height = img.size
        # print(width, height)
        print("Processing map...")
        envS.theMap, envS.playerPosition = envS.processImage(img)
        print("Image captured and processed.")
      if(option == "2"):
        if(envS.theMap != []):        
          print("moving...")
          #path = self.agent.pathToClosestDiamond()
          path = self.agent.compute(envS.theMap)
          envS.theMap, envS.playerPosition = envS.mapPlayerMove(
              envS.theMap, envS.playerPosition, path, envS.driver)
          # envS.playerPosition = envS.play(
          #    envS.theMap, envS.playerPosition, envS.driver)
        else:
          print("Use option 1 to capture the map before select option 2")
        print("Game played")
      if(option == "3"):
        envS.driver.quit()
        envS.playing = False


if __name__ == "__main__":
  test = Environment()
  test.execute()
