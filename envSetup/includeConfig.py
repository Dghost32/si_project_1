# PILLOW
from PIL import Image
# SELENIUM
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
# CHROME DRIVER
from webdriver_manager.chrome import ChromeDriverManager
# Import src
from src.utils.imageProcessor import processImage
from src.utils.game import play, mapPlayerMove
from src.seleniumWebTools.seleniumOptions import chromeOptions

# ---------------------CONSTANT VARIABLES---------------------
URL = "https://www.minijuegosgratis.com/v3/games/games/prod/219431/diamond-rush/index.html"

# ---------------------SELENIUM SETUP---------------------
chromeDriver = ChromeDriverManager().install()
driver = webdriver.Chrome(chromeDriver, options=chromeOptions)
driver.get(URL)
gameCanvas = driver.find_element(By.TAG_NAME, "canvas")

# ---------------------GLOBAL VARIABLES---------------------
global playing
playing = True

global theMap
theMap = []

global playerPosition
playerPosition = [0, 0]
