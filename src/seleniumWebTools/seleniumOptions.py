# OS
from src.utils.getUserData import userData
import os
import sys
# SELENIUM
from selenium import webdriver
# Import utils
sys.path.insert(0, f"{os.getcwd()}/src/utils")

chromeOptions = webdriver.ChromeOptions()
chromeOptions.add_argument(f"user-data-dir={userData}")
chromeOptions.add_argument("--disable-extensions")
chromeOptions.add_argument("--disable-infobars")
chromeOptions.add_argument("--window-position=0,0")
chromeOptions.add_argument("--window-size=640,960")
