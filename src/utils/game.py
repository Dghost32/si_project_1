from envSetup.includeConfig import Keys
# import Action chains
from selenium.webdriver.common.action_chains import ActionChains
# NUMPY
import numpy as np
# python
import random
import time


def updateMap(map, player, direction):
  if(direction == "U"):
    map[player[0]][player[1]] = "F"
    map[player[0] - 1][player[1]] = "P"
    player = [player[0] - 1, player[1]]

  if(direction == "D"):
    map[player[0]][player[1]] = "F"
    map[player[0] + 1][player[1]] = "P"
    player = [player[0] + 1, player[1]]

  if(direction == "L"):
    map[player[0]][player[1]] = "F"
    map[player[0]][player[1] - 1] = "P"
    player = [player[0], player[1] - 1]

  if(direction == "R"):
    map[player[0]][player[1]] = "F"
    map[player[0]][player[1] + 1] = "P"
    player = [player[0], player[1] + 1]

  return map, player


def updateGame(direction, driver):
  keyWait = 0.2
  action = ActionChains(driver)
  if direction == "R":
    time.sleep(keyWait)
    action.key_down(Keys.RIGHT).perform()
    time.sleep(keyWait)
    action.key_up(Keys.RIGHT).perform()
  if direction == "L":
    time.sleep(keyWait)
    action.key_down(Keys.LEFT).perform()
    time.sleep(keyWait)
    action.key_up(Keys.LEFT).perform()
  if direction == "U":
    time.sleep(keyWait)
    action.key_down(Keys.UP).perform()
    time.sleep(keyWait)
    action.key_up(Keys.UP).perform()
  if direction == "D":
    time.sleep(keyWait)
    action.key_down(Keys.DOWN).perform()
    time.sleep(keyWait)
    action.key_up(Keys.DOWN).perform()


def isMoveValid(map, player, direction):
  # floor, diamond, spikes, key, trophy, button, exitOpen
  availableMoves = ["F", "D", "S", "K", "T", "B", "EO"]
  valid = False

  if direction == "U":
    if(map[player[0] - 1][player[1]] in availableMoves):
      valid = True

  if direction == "D":
    if(map[player[0] + 1][player[1]] in availableMoves):
      valid = True

  if direction == "L":
    if(map[player[0]][player[1] - 1] in availableMoves):
      valid = True

  if direction == "R":
    if(map[player[0]][player[1] + 1] in availableMoves):
      valid = True

  return valid


def validateDirection(direction):
  availableDirections = ["L", "R", "D", "U"]
  return direction not in availableDirections


def mapPlayerMove(map, player, moves, driver):
  #print(f"initial map is \n${np.matrix(map)}")
  for move in moves:
    pressAmount = move[0]
    direction = move[1]
    while(pressAmount > 0):
      # if isMoveValid(map, player, direction):
      if True:
        updateGame(direction, driver)
        #map, player = updateMap(map, player, direction)
      pressAmount = pressAmount - 1
  #print(f"final map is \n${np.matrix(map)}")
  return map, player


def movePlayer(map, player, direction, driver):
  action = ActionChains(driver)
  # floor, diamond, spikes, key, trophy, button, exitOpen
  availableMoves = ["F", "D", "S", "K", "T", "B", "EO", "E"]
  keyWait = 0.2
  error = False
  if(direction == "up"):
    if(map[player[0] - 1][player[1]] in availableMoves):
      map[player[0]][player[1]] = "F"
      map[player[0] - 1][player[1]] = "P"
      player = [player[0] - 1, player[1]]
      # move player
      action.key_down(Keys.UP).perform()
      time.sleep(keyWait)
      action.key_up(Keys.UP).perform()
    else:
      error = True

  if(direction == "down"):
    if(map[player[0] + 1][player[1]] in availableMoves):
      map[player[0]][player[1]] = "F"
      map[player[0] + 1][player[1]] = "P"
      player = [player[0] + 1, player[1]]
      # move player
      action.key_down(Keys.DOWN).perform()
      time.sleep(keyWait)
      action.key_up(Keys.DOWN).perform()
    else:
      error = True

  if(direction == "left"):
    if(map[player[0]][player[1] - 1] in availableMoves):
      map[player[0]][player[1]] = "F"
      map[player[0]][player[1] - 1] = "P"
      player = [player[0], player[1] - 1]
      # move player
      action.key_down(Keys.LEFT).perform()
      time.sleep(keyWait)
      action.key_up(Keys.LEFT).perform()
    else:
      error = True

  if(direction == "right"):
    if(map[player[0]][player[1] + 1] in availableMoves):
      map[player[0]][player[1]] = "F"
      map[player[0]][player[1] + 1] = "P"
      player = [player[0], player[1] + 1]
      # move player
      action.key_down(Keys.RIGHT).perform()
      time.sleep(keyWait)
      action.key_up(Keys.RIGHT).perform()
    else:
      error = True
  return map, player, error


def play(map, playerPosition, driver):
  print(f"Initial map: \n {np.matrix(map)}")
  print(f"Initial player position: \n {playerPosition}")
  # move player randomly
  options = ["right", "up", "left", "down"]
  movesCounter = 0
  for _ in range(40):
    rand = random.randint(0, 3)
    direction = options[rand]
    time.sleep(0.2)
    map, playerPosition, error = movePlayer(
        map, playerPosition, direction, driver)
  if(not error):
    movesCounter += 1

  # print(f"Final map: \n {np.matrix(map)}")
  # print(f"Moves are: \n {np.array(moves)}")
  print(f"Final player position: \n {playerPosition}")
  print(f"movesCounter: {movesCounter}")
  # print(response)
  return playerPosition
