# OS
import platform
import os

# get Os
system = platform.system()

# get ~
userHome = os.path.expanduser("~")
# get user data -> chrome session
userData = f"{userHome}/.config/google-chrome/Default"
if(system == "Windows"):
  userData = f"{userHome}/AppData/Local/Google/Chrome/User Data"
if(system == "Darwin"):
  userData = f"{userHome}/Library/Application Support/Google/Chrome/Default"
