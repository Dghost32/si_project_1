import os
from skimage.metrics import structural_similarity
import cv2
import numpy as np


def compareImages(imageA, imageB):
  """ Compute the mean squared error and structural similarity
  index for the images """
  # convert the images to grayscale
  imageA = cv2.cvtColor(imageA, cv2.COLOR_BGR2GRAY)
  imageB = cv2.cvtColor(imageB, cv2.COLOR_BGR2GRAY)
  # resize images so they are the same dimensions
  imageA = cv2.resize(imageA, (32, 32))
  imageB = cv2.resize(imageB, (32, 32))
  return structural_similarity(imageA, imageB)


def sortImage(image):
  """ Compares an image with saved tilesets to
  find what type of tile it is """
  maxConfidence = [0, ""]

  for folder in os.listdir("./media/assets"):
    for file in os.listdir(f"./media/assets/{folder}"):
      tile = cv2.imread(f"./media/assets/{folder}/{file}", 1)
      confidence = compareImages(image, tile)
      if confidence > maxConfidence[0]:
        maxConfidence = [confidence, folder]
  type = maxConfidence[1]
  return type


def processImage(image):
  """ Turns a map into a 10x15 matrix """
  width, height = image.size
  xStep, yStep = width / 10, height / 15
  matrix = []
  playerPosition = [0, 0]

  for y in range(0, 15):
    row = []
    for x in range(0, 10):
      tile = (x * xStep, y * yStep, (x + 1) * xStep, (y + 1) * yStep)
      image.crop(tile).save(f"./media/tiles/{y}_{x}.png")
      tileImage = cv2.imread(f"./media/tiles/{y}_{x}.png", 1)
      element = sortImage(tileImage)
      row.append(element)
      if(element == "P" or element == "PK"):
        playerPosition = [y, x]
    matrix.append(row)

  print("Finished: Image processing")
  print(np.matrix(matrix))
  return matrix, playerPosition
